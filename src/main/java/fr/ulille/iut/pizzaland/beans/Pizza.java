package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
    private UUID id = UUID.randomUUID();
    private String name;
    private List<Ingredient> ingredients;

    public Pizza() {
    }

    public Pizza(String name, List<Ingredient> ingredients) {
        this.name = name;
        this.ingredients=ingredients;
    }

	public Pizza(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   /* public boolean equals(Pizza p) {
		return id.toString().equals(p.getId().toString()) && name.equals(p.getName());
	}*/

	public static PizzaDto toDto(Pizza i) {
        PizzaDto dto = new PizzaDto();
        dto.setId(i.getId());
        dto.setName(i.getName());
        dto.setIngredients(i.getIngredients());

        return dto;
    }
    
    @Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	public static Pizza fromDto(PizzaDto dto) {
        Pizza pizza = new Pizza();
        pizza.setId(dto.getId());
        pizza.setName(dto.getName());

        return pizza;
    }
    
    public static PizzaCreateDto toCreateDto(Pizza pizza) {
    	PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
            
        return dto;
      }
    	
      public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
    	  Pizza pizza = new Pizza();
        pizza.setName(dto.getName());

        return pizza;
      }


    @Override
    public String toString() {
        return "Pizza [id=" + id + ", name=" + name + "]";
    }
}
