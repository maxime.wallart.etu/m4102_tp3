package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	private List<Ingredient> ingredients;
		
	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}
}
